# REST service security reporter
[![pipeline status](https://gitlab.com/chvote2/shared-libraries/rest-service-security-reporter/badges/master/pipeline.svg)](https://gitlab.com/chvote2/web-apps/chvote-boshared-libraries/rest-service-security-reporter/commits/master)

Maven plugin that generates a csv report of the SpringMVC REST endpoints describing
the paths and the roles that are authorized.

Currently only used by the chvote-bo buils pipeline.