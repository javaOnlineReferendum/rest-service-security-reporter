/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.security;

import static java.util.Optional.ofNullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ControllerVisitsResults {


  List<ControllerVisitResult> controllerVisitResults = new ArrayList<>();


  InProgressControllerVisitResult current;


  public InProgressControllerVisitResult current() {
    return current;
  }


  public void addNewController(String className) {
    current = new InProgressControllerVisitResult(className);


  }


  @Override
  public String toString() {
    return controllerVisitResults.stream().map(Object::toString).collect(Collectors.joining("\n"));
  }

  public void flush() {
    if (current != null) {
      current.flush();
    }
  }

  public class InProgressControllerVisitResult {
    private final String         className;
    private       RequestMapping baseRequestMapping;
    private Set<String> baseRoles = new HashSet<>();
    private RequestMapping methodRequestMapping;
    private Set<String> methodRoles = new HashSet<>();

    public void setBaseRoles(Set<String> baseRoles) {
      this.baseRoles = baseRoles;
    }

    public void setMethodRoles(Set<String> methodRoles) {
      this.methodRoles = methodRoles;
    }

    public InProgressControllerVisitResult(String className) {
      this.className = className;
    }

    public void setBaseRequestMapping(RequestMapping baseRequestMapping) {
      this.baseRequestMapping = baseRequestMapping;
    }

    public void setMethodRequestMapping(RequestMapping methodRequestMapping) {
      this.methodRequestMapping = methodRequestMapping;
    }

    public void flush() {
      if (methodRequestMapping == null) {
        return;
      }


      String path = "/" + ofNullable(baseRequestMapping)
          .flatMap(rm -> ofNullable(rm.path))
          .map(p -> p + "/").orElse("") + ofNullable(methodRequestMapping.path).orElse("");


      String method =
          ofNullable(methodRequestMapping.method)
              .orElse(ofNullable(baseRequestMapping).flatMap(m -> ofNullable(m.method)).orElse("ALL"));

      controllerVisitResults.add(new ControllerVisitResult(
          path, method, className,
          Stream.concat(baseRoles.stream(), methodRoles.stream()).collect(Collectors.toSet())));


      methodRequestMapping = null;
      methodRoles = new HashSet<>();
    }
  }

  public static class ControllerVisitResult {
    public final String      path;
    public final String      method;
    public final String      className;
    public final Set<String> roles;

    private ControllerVisitResult(String path, String method, String className, Set<String> roles) {
      this.path = path.replaceAll("\"", "")
                      .replaceAll("/+", "/");

      this.method = method;
      this.className = className;
      this.roles = roles;
    }

    @Override
    public String toString() {
      return String.format("%s path=%s, method=%s", className, path, method);
    }
  }
}
